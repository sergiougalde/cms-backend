package com.cencosud.sim.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.cencosud.sim.model.response.ResEmpresaResponse;
import com.cencosud.sim.service.FileUploadService;

/**
 * Su principal fin es subir un archivo excel y procesarlo para registrar la respuesta
 * de las companias
 * 
 * @author daniel
 *
 */
@RestController
@RequestMapping("/rest")
public class FileUploadController {
	static final Logger log = Logger.getLogger(FileUploadController.class);
	
	@Autowired
	@Qualifier("FileUploadService")
	private FileUploadService fileUploadService;

   @CrossOrigin(origins = "*")
   @PostMapping("/upload")
   public ResEmpresaResponse uploadFile(MultipartHttpServletRequest request){
	   log.info("INGRESA A ===>  uploadFile("+request.getParameterValues("empresa")[0]+")");
    return fileUploadService.uploadFile(request);
   }
   
}