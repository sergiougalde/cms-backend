package com.cencosud.sim.controller.intranet;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/rest/api/intranet")
@CrossOrigin(origins = "*")
public interface BaseServiciosIntranet {
	final static Logger logger = Logger.getLogger(BaseServiciosIntranet.class);
}
