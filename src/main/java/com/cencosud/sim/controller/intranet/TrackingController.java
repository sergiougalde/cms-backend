package com.cencosud.sim.controller.intranet;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cencosud.sim.dto.Accion;
import com.cencosud.sim.dto.Tracking;

@RestController
public class TrackingController implements BaseServiciosIntranet {
	
	@GetMapping("getTracking")
	public List<Tracking> getTransacciones() {
		List<Tracking> list = new ArrayList<Tracking>();
		
		Tracking track = new Tracking();
		Accion accion = new Accion();
		accion.setNombre("VALIDAR");
		accion.setUrl("");
		
		track.setId(3177);
		track.setDescripcion("CARGA WORKFLOW");
		track.setFecha(new Date(Calendar.getInstance().getTime().getTime()));
		
		track.setPendiente(37);
		track.setUsuario("VGONZALEZ");
		track.setSimOk(0);
		track.setSimNok(0);
		track.setDigitalNok(0);
		track.setDigitalOk(0);
		track.setManualOk(0);
		track.setManualNok(0);
		track.setFileShare(0);
		
		list.add(track);
		
		return list;
	}
}
