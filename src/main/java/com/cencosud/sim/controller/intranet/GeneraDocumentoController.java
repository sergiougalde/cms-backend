package com.cencosud.sim.controller.intranet;

import java.util.ArrayList;
import java.util.Calendar;
import java.sql.Date;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cencosud.sim.dto.Accion;
import com.cencosud.sim.dto.Transaccion;

@RestController
public class GeneraDocumentoController implements BaseServiciosIntranet {
	
	@GetMapping("getTransacciones")
	public List<Transaccion> getTransacciones() {
		List<Transaccion> list = new ArrayList<Transaccion>();
		
		Transaccion tx = new Transaccion();
		Accion accion = new Accion();
		accion.setNombre("VALIDAR");
		accion.setUrl("");
		
		tx.setId(3177);
		tx.setFecha(new Date(Calendar.getInstance().getTime().getTime()));
		tx.setDescripcion("PROCESO CARGA VENTAS CON INSPECCION");
		tx.setPendiente(37);
		tx.setEstado("CARGA MASIVA");
		tx.setAccion(accion);
		
		list.add(tx);
		
		return list;
	}
}
