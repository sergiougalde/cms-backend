package com.cencosud.sim.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.cencosud.sim.model.request.CargaPcbsRequest;
import com.cencosud.sim.model.response.MessageResponse;
import com.cencosud.sim.service.GeneracionDocumentoService;
import com.cencosud.sim.service.TransaccionService;

@RestController
@RequestMapping("/rest")
public class GeneracionDocumentoController {
	static final Logger log = Logger.getLogger(GeneracionDocumentoController.class);
	
	@Autowired
	@Qualifier("GeneracionDocumentoService")
	private GeneracionDocumentoService generacionDocumentoService;
	
	@Autowired
	@Qualifier("TransaccionService")
	private TransaccionService transaccionService;
	
	@CrossOrigin(origins = "*")
	@PostMapping("/validarCarga")
	public MessageResponse validarCarga(@RequestBody CargaPcbsRequest cargaPcbsRequest) {
		log.info("INGRESA A ===> validarCarga("+cargaPcbsRequest+")");
		return generacionDocumentoService.validarCarga(cargaPcbsRequest);
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping("/procesarDocumentos")
	public MessageResponse procesarDocumentos(@RequestBody CargaPcbsRequest cargaPcbsRequest) {
		log.info("INGRESA A ===> procesarDocumentos("+cargaPcbsRequest+")");
		return generacionDocumentoService.procesarDocumentos(cargaPcbsRequest);
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping("/generarDocumentos")
	public MessageResponse generarDocumentos(@RequestBody CargaPcbsRequest cargaPcbsRequest) {
		log.info("INGRESA A ===> generarDocumentos("+cargaPcbsRequest+")");
		return generacionDocumentoService.generarDocumentos(cargaPcbsRequest);
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping("/reValidar")
	public MessageResponse reValidar(@RequestBody CargaPcbsRequest cargaPcbsRequest) {
		log.info("INGRESA A ===> reValidar("+cargaPcbsRequest+")");
		return generacionDocumentoService.reValidar(cargaPcbsRequest);
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping("/distribuir")
	public MessageResponse distribuir(@RequestBody CargaPcbsRequest cargaPcbsRequest) {
		log.info("INGRESA A ===> distribuir("+cargaPcbsRequest+")");
		return generacionDocumentoService.distribuir(cargaPcbsRequest);
	}
	
}
