package com.cencosud.sim.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cencosud.sim.mapper.TipoTransaccionMapper;
import com.cencosud.sim.model.TipoTransaccionModel;
import com.cencosud.sim.service.TipoTransaccionService;

@Service("TipoTransaccionService")
public class TipoTransaccionServiceImpl implements TipoTransaccionService{

	@Autowired
	private TipoTransaccionMapper tipoTransaccionMapper;
	
	@Override
	public List<TipoTransaccionModel> obtenerTipoTransaccion() {
		return tipoTransaccionMapper.obtenerTipoTransaccion();
	}

}
