package com.cencosud.sim.service.impl;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.cencosud.sim.constants.Constants;
import com.cencosud.sim.model.RecepcionModel;
import com.cencosud.sim.model.RespuestaModel;
import com.cencosud.sim.model.TransaccionModel;
import com.cencosud.sim.model.request.CargaPcbsRequest;
import com.cencosud.sim.model.response.ResEmpresaResponse;
import com.cencosud.sim.service.CargaPcbsService;
import com.cencosud.sim.service.FileUploadService;
import com.cencosud.sim.service.RecepcionService;
import com.cencosud.sim.service.RespuestaService;
import com.cencosud.sim.service.TransaccionService;
import com.cencosud.sim.util.ArchivoServidor;
import com.cencosud.sim.util.LeerArchivosExcel;

@Service("FileUploadService")
public class FileUploadServiceImpl implements FileUploadService{
	static final Logger log = Logger.getLogger(FileUploadServiceImpl.class);
	
	@Autowired
	@Qualifier("RecepcionService")
	private RecepcionService recepcionService;
	
	@Autowired
	@Qualifier("TransaccionService")
	private TransaccionService transaccionService;
	
	@Autowired
	@Qualifier("RespuestaService")
	private RespuestaService respuestaService;
	
	@Autowired
	@Qualifier("CargaPcbsService")
	private CargaPcbsService cargaPcbsService;
	
	/**
	 * Sube archivo seleccionado y lo procesa segun la extension que tenga
	 */
	@Override
	public ResEmpresaResponse uploadFile(MultipartHttpServletRequest request) {
		ResEmpresaResponse respuesta = new ResEmpresaResponse();
		Map<String,Object> contenido = null;
		try {
			
			String valorEmpresa = request.getParameterValues("empresa")[0];
			String usuario = request.getParameterValues("usuario")[0];
			int empresa = Integer.parseInt(valorEmpresa!=null && !valorEmpresa.equals("")?valorEmpresa:"0");
			
			Iterator<String> itr = request.getFileNames();
			MultipartFile file = request.getFile(itr.next());
			String originalFileName = file.getOriginalFilename();
			String[] fileNameSplit = originalFileName.split("\\.");
			String extension = fileNameSplit[fileNameSplit.length -1];
			ArchivoServidor archivo = new ArchivoServidor();
			File archivoExcel = archivo.archivoTemporal(file); 
			
			contenido = leerArchivo(extension,archivoExcel);
			
			respuesta = procesarArchivo(contenido,archivoExcel,empresa,usuario);

			if(archivoExcel.delete()) {
				log.info("ARCHIVO TEMPORAL BORRADO : "+archivoExcel.getAbsolutePath());
			}
			
		} catch (Exception e) {
			log.error(e);
			respuesta = new ResEmpresaResponse();
			respuesta.setError(true);
			respuesta.setDescripcion(Constants.EXCEL_MENSAJE_ERROR_INCONSISTENTE);
		}
		
		return respuesta;
	}
	

	public Map<String, Object> leerArchivo(String extension, File archivoExcel) {
		Map<String,Object> contenido = null;
		LeerArchivosExcel excel = new  LeerArchivosExcel();
		/*
		 * Una vez que se ha obtenido el archivo, se hace una distinción en el formato que 
		 * este tiene, para que sea procesado por la libreria poi
		 */
		if(Constants.EXTENSION_EXCEL_XLS.equalsIgnoreCase(extension) 
				|| Constants.EXTENSION_EXCEL_XLSX.equalsIgnoreCase(extension)) {
			contenido = excel.leerExcel(archivoExcel,extension);
		}else {
			log.info("LA EXTENCIÓN DEL ARCHIVO NO CORRESPONDE");
		}
		
		return contenido;
	}


	@SuppressWarnings("unchecked")
	private ResEmpresaResponse procesarArchivo(Map<String, Object> contenido, File archivoExcel, int empresa, String usuario) {
		LeerArchivosExcel excel = new  LeerArchivosExcel();
		List<Map<String,Object>> lectura = (List<Map<String, Object>>) contenido.get("lectura");
		Map<Integer,String> encabezado = (Map<Integer,String>) contenido.get("clave");
		String nombreArchivo = new Date().hashCode()+"_"+obtenerNombreReal(archivoExcel);
		String ruta = Constants.EXCEL_RUTA_RESPUESTA+nombreArchivo;
		
		ResEmpresaResponse respuesta = excel.validarFormatoArchivo(encabezado.toString(), empresa, lectura);
		int leidos = respuesta.getLeido();
		if(leidos>0) {
			int recId = procesarArchivos(archivoExcel,ruta,leidos,lectura,empresa);
			
			if(empresa == Constants.EMPRESA_CORREO_ELECTRONICO && respuesta.getNoEntregado() > 0) {
				procesarRegistrosId(usuario,recId);
			}
			
		}
		return respuesta;
	}

	private void procesarRegistrosId(String usuario, int recId) {
		
		List<RespuestaModel> respuestas = respuestaService.obtenerRespuestasEmail(recId);
		
		LinkedHashSet<Integer> txIdList = new LinkedHashSet<>();
		for (RespuestaModel respuestaModel : respuestas) {
			txIdList.add(respuestaModel.getRespTxId());
		}
		
		LinkedHashSet<Integer> tiposTransaccion = new LinkedHashSet<>();
		for (Integer txId : txIdList) {
			TransaccionModel transaccion = transaccionService.obtenerTransaccionPadre(txId);
			tiposTransaccion.add(transaccion.getTptxId());
			for(int i = 0; i < respuestas.size(); i++) {
				if(respuestas.get(i).getRespTxId() == txId) {
					respuestas.get(i).setTptxId(transaccion.getTptxId());
				}
			}
		}
		
		for (Integer tptxId : tiposTransaccion) {
			dividirRegistros(tptxId,respuestas,usuario);
		}
		
	}

	private void dividirRegistros(Integer tptxId, List<RespuestaModel> respuestas, String usuario) {
		List<RespuestaModel> respuestasList = new ArrayList<>();
		for (RespuestaModel respuesta : respuestas) {
			if(respuesta.getTptxId() == tptxId) {
				respuestasList.add(respuesta);
			}
		}
		registrarTransaccionEmail(respuestasList, tptxId,usuario);
		
	}


	private void registrarTransaccionEmail(List<RespuestaModel> respuestas, Integer tptxId, String usuario) {
		CargaPcbsRequest cargaPcbsVerificada = new CargaPcbsRequest();
		cargaPcbsVerificada.setEstado(Constants.PENDIENTE);
		cargaPcbsVerificada.setTptxId(tptxId);
		cargaPcbsVerificada.setUsuario(usuario);
		
		TransaccionModel transaccionModel = transaccionService.registrarTransaccion(cargaPcbsVerificada);
		cargaPcbsVerificada.setTxId(transaccionModel.getTxId());
		
		int nuevoId = transaccionModel.getTxId();
		
		cargaPcbsService.registrarIdEmail(respuestas,nuevoId);
	
		
		transaccionModel.setTxIdPadre(transaccionModel.getTxId());
		//Guarda la transacción
		transaccionService.guardarTransaccion(transaccionModel);
		
	}


	private int procesarArchivos(File archivoExcel, String ruta, int leidos, List<Map<String, Object>> lectura, int empresa) {
		ArchivoServidor archivo = new ArchivoServidor();
		/* 
		 * Guarda el archivo en el servidor mediante smb, esta ruta es 
		 * configurada en el archivo de propiedades
		 * */
		archivo.guardarArchivoSmb(archivoExcel,ruta); 
		/*
		 * Registra el archivo guardado en la tabla recepcion
		 */
		int recId = registrarArchivo(archivoExcel,ruta,leidos,empresa);
		/*
		 * para el caso de correo electronico, se agregara el id de la tabla recepcion
		 * y un listado de id de la tabla carga_pcbs que se podrían utilizar
		 */
		procesarFlujoRespuesta(lectura,empresa,recId);
		
		return recId;
	}


	private void procesarFlujoRespuesta(List<Map<String, Object>> lectura, int empresa, int recId) {
		try {
			List<RespuestaModel> respuestas = agregarData(lectura,empresa,recId);
			respuestaService.registrarRespuesta(respuestas,recId,empresa);
		} catch (Exception e) {
			log.error(e);
		}
	}
	
	public List<RespuestaModel> agregarData(List<Map<String, Object>> lectura, int empresa, int recId) {
		log.info("AGREGAR DATA ");
		List<RespuestaModel> respuestas = null;
		try {
			if(empresa != Constants.EMPRESA_CORREO_ELECTRONICO) {
				respuestas = new ArrayList<>();
				for (Map<String, Object> map : lectura) {
					respuestas.add(llenarTradicional(map));
				}
			}else {
				respuestas = new ArrayList<>();
				for (Map<String, Object> map : lectura) {
					respuestas.add(llenarElectronico(map));
				}
			}
			
			for (int i = 0; i < respuestas.size(); i++) {
				String[] refDescompuesta = respuestas.get(i).getRespReferencia().split("-");
				int pptaId = Integer.parseInt(refDescompuesta[1]);
				int txId = Integer.parseInt(refDescompuesta[2]);
				respuestas.get(i).setRespPptaId(pptaId);
				respuestas.get(i).setRespTxId(txId);
				respuestas.get(i).setRecId(recId);
				log.info(respuestas.get(i));
			}
		} catch (Exception e) {
			log.error(e);
		}
		return respuestas;
	}
	
	private RespuestaModel llenarElectronico(Map<String, Object> map) {
		RespuestaModel respuesta = new RespuestaModel();
		for (Map.Entry<String, Object> entry : map.entrySet()){
			try {
				if (Constants.EXCEL_ENCABEZADO_ELECTRONICO_HORA_ENVIO.equals(entry.getKey())) {
					String fecha = map.get(Constants.EXCEL_ENCABEZADO_ELECTRONICO_FECHA_ENVIO).toString();
					respuesta.setRespFchEnvio(obtenerDateTime(fecha,entry));
				} else if (Constants.EXCEL_ENCABEZADO_ELECTRONICO_ENVIADO.equals(entry.getKey())) {
					respuesta.setRespEnviado(obtenerString(entry));
				} else if (Constants.EXCEL_ENCABEZADO_ELECTRONICO_CORREO.equals(entry.getKey())) {
					respuesta.setRespCorreo(obtenerString(entry));
				} else if (Constants.EXCEL_ENCABEZADO_ELECTRONICO_CODIGO_PDF.equals(entry.getKey())) {
					respuesta.setRespCodigoPdf(obtenerInteger(entry));
				} else if (Constants.EXCEL_ENCABEZADO_ELECTRONICO_SMTP.equals(entry.getKey())) {
					respuesta.setRespSmtp(obtenerString(entry));
				} else if (Constants.EXCEL_ENCABEZADO_ELECTRONICO_ESTADO.equals(entry.getKey())) {
					respuesta.setRespEstado(obtenerString(entry));
				} else if (Constants.EXCEL_ENCABEZADO_ELECTRONICO_CODIGO_BARRA.equals(entry.getKey())) {
					respuesta.setRespReferencia(obtenerString(entry));
				} else if (Constants.EXCEL_ENCABEZADO_ELECTRONICO_TIPO_REBOTE.equals(entry.getKey())) {
					respuesta.setRespTipoRebote(obtenerString(entry));
				} else if (Constants.EXCEL_ENCABEZADO_ELECTRONICO_REGLA_REBOTE.equals(entry.getKey())) {
					respuesta.setRespReglaRebote(obtenerString(entry));
				} else if (Constants.EXCEL_ENCABEZADO_ELECTRONICO_FECHA_REBOTE.equals(entry.getKey())) {
					respuesta.setRespFchRebote(obtenerDate(entry));
				} else if (Constants.EXCEL_ENCABEZADO_ELECTRONICO_FECHA_RECEPCION.equals(entry.getKey())) {
					respuesta.setRespFchRecepcion(obtenerDate(entry));
				} 
			} catch (Exception e) {
				log.error(e);
			}
		}
		return respuesta;
	}

	public RespuestaModel llenarTradicional(Map<String, Object> map) {
		RespuestaModel respuesta = new RespuestaModel();
		for (Map.Entry<String, Object> entry : map.entrySet()){
			try {
				if (Constants.EXCEL_ENCABEZADO_TRADICIONAL_ENVIO.equals(entry.getKey())) {
					respuesta.setRespEnvio(obtenerLong(entry));
				} else if (Constants.EXCEL_ENCABEZADO_TRADICIONAL_REFERENCIA.equals(entry.getKey())) {
					respuesta.setRespReferencia(obtenerString(entry));
				} else if (Constants.EXCEL_ENCABEZADO_TRADICIONAL_ESTADO.equals(entry.getKey())) {
					respuesta.setRespEstado(obtenerString(entry));
				} else if (Constants.EXCEL_ENCABEZADO_TRADICIONAL_INCIDENCIA.equals(entry.getKey())) {
					respuesta.setRespIncidencia(obtenerString(entry));
				} else if (Constants.EXCEL_ENCABEZADO_TRADICIONAL_FECHAADMISION.equals(entry.getKey())) {
					respuesta.setRespFchAdmision(obtenerDate(entry));
				} else if (Constants.EXCEL_ENCABEZADO_TRADICIONAL_FECHAENTREGA.equals(entry.getKey())) {
					respuesta.setRespFchEntrega(obtenerDate(entry));
				} else if (Constants.EXCEL_ENCABEZADO_TRADICIONAL_NOMBRE.equals(entry.getKey())) {
					respuesta.setRespNombre(obtenerString(entry));
				} else if (Constants.EXCEL_ENCABEZADO_TRADICIONAL_DIRECCION.equals(entry.getKey())) {
					respuesta.setRespDireccion(obtenerString(entry));
				} else if (Constants.EXCEL_ENCABEZADO_TRADICIONAL_COMUNA.equals(entry.getKey())) {
					respuesta.setRespComuna(obtenerString(entry));
				} else if (Constants.EXCEL_ENCABEZADO_TRADICIONAL_OT.equals(entry.getKey())) {
					respuesta.setRespOt(obtenerInteger(entry));
				} else if (Constants.EXCEL_ENCABEZADO_TRADICIONAL_IDCLIENTE.equals(entry.getKey())) {
					respuesta.setRespIdCliente(obtenerInteger(entry));
				} else if (Constants.EXCEL_ENCABEZADO_TRADICIONAL_PROYECTO.equals(entry.getKey())) {
					respuesta.setRespProyecto(obtenerString(entry));
				} else if (Constants.EXCEL_ENCABEZADO_TRADICIONAL_COR.equals(entry.getKey())) {
					respuesta.setRespCor(obtenerInteger(entry));
				} else if (Constants.EXCEL_ENCABEZADO_TRADICIONAL_SERVICIO.equals(entry.getKey())) {
					respuesta.setRespServicio(obtenerString(entry));
				}
			} catch (Exception e) {
				log.error(e);
			}
		}
		return respuesta;
	}
	
	private Long obtenerLong(Entry<String, Object> entry) {
		Long respuesta = null;
		if(entry.getValue() instanceof Integer && entry.getValue()!=null) {
			respuesta = Long.parseLong(entry.getValue().toString());
		}
		return respuesta;
	}


	private String obtenerString(Map.Entry<String, Object> entry) {
		String respuesta = null;
		if(entry.getValue() instanceof String && entry.getValue()!=null) {
			respuesta = entry.getValue().toString();
		}
		return respuesta;
	}
	
	private int obtenerInteger(Map.Entry<String, Object> entry) {
		int respuesta = 0;
		if(entry.getValue() instanceof Integer && entry.getValue()!=null) {
			respuesta = Integer.parseInt(entry.getValue().toString());
		}
		return respuesta;
	}
	
	private Date obtenerDate(Map.Entry<String, Object> entry) {
		Date respuesta = null;
		if(entry.getValue() instanceof String && entry.getValue()!=null) {
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			try {
				respuesta = formatter.parse(entry.getValue().toString());
			} catch (ParseException e) {
				log.error(e);
			}
		}
		return respuesta;
	}
	


	private Date obtenerDateTime(String fecha, Entry<String, Object> entry) {
		Date respuesta = null;
		if(entry.getValue() instanceof String && entry.getValue()!=null) {
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			try {
				respuesta = formatter.parse(fecha + " " +entry.getValue().toString());
			} catch (ParseException e) {
				log.error(e);
			}
		}
		return respuesta;
	}

	/**
	 * Registra archivo de respuesta correspondiente a las empresas en la tabla recepcion
	 * @param archivoExcel
	 * @param ruta
	 * @param leidos
	 * @param empresa
	 * @return 
	 */
	private int registrarArchivo(File archivoExcel, String ruta, int leidos, int empresa) {
		String nombreArchivo = obtenerNombreReal(archivoExcel);
		String rutaArchivo = ruta.replaceAll("smb:", "");
		RecepcionModel recepcionModel = new RecepcionModel();
		recepcionModel.setEmpresa(empresa);
		recepcionModel.setRecRegistros(leidos);
		recepcionModel.setRecRutaArchivo(rutaArchivo);
		recepcionModel.setRecNombreArchivo(nombreArchivo);
		
		log.info("nombreArchivo : "+nombreArchivo);
		log.info("rutaArchivo : "+rutaArchivo);
		log.info("leidos : "+leidos);
		log.info("empresa : "+empresa);
		log.info(recepcionModel);
		recepcionService.registrarArchivoRespuesta(recepcionModel);
		
		return recepcionModel.getRecId();
	}


	private String obtenerNombreReal(File archivoExcel) {
		String[] nombreArchivoDesc = archivoExcel.getName().split("\\.");
		String nombreArchivoReal = archivoExcel.getName().split("-SIM-")[0];
		String extension = nombreArchivoDesc[nombreArchivoDesc.length - 1];
		return nombreArchivoReal+"."+extension;
	}

}

