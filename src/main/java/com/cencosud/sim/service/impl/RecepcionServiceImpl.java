package com.cencosud.sim.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cencosud.sim.constants.Constants;
import com.cencosud.sim.mapper.RecepcionMapper;
import com.cencosud.sim.model.RecepcionModel;
import com.cencosud.sim.model.request.HistorialRequest;
import com.cencosud.sim.service.RecepcionService;
import com.cencosud.sim.util.Util;

@Service("RecepcionService")
public class RecepcionServiceImpl implements RecepcionService{
		static final Logger log = Logger.getLogger(RecepcionServiceImpl.class);
	
		@Autowired
		private RecepcionMapper recepcionMapper;

		@Override
		public int registrarArchivoRespuesta(RecepcionModel recepcionModel) {
			int respuesta = 0;
			try {
				respuesta = recepcionMapper.registrarArchivoRespuesta(recepcionModel);
			} catch (Exception e) {
				log.error(e);
			}
			return respuesta;
		}

		@Override
		public List<RecepcionModel> obtenerHistorial(HistorialRequest historialRequest) {
			List<RecepcionModel> historial = null;
			try {
				Util util = new Util();
				if(historialRequest.getDesde() != null){
					historialRequest.setDesde(util.convertirFecha(historialRequest.getDesde())+Constants.HORA_INICIO);
					historialRequest.setHasta(util.convertirFecha(historialRequest.getHasta())+Constants.HORA_FIN);
				}
				historial = recepcionMapper.obtenerHistorial(historialRequest);
			} catch (Exception e) {
				log.error(e);
			}
			return historial;
		}

		@Override
		public RecepcionModel obtenerRecepcion(int recId) {
			RecepcionModel recepcion = null;
			try {
				recepcion = recepcionMapper.obtenerRecepcion(recId);
			} catch (Exception e) {
				log.error(e);
			}
			return recepcion;
		}
		
}
