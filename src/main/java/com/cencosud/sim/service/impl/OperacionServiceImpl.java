package com.cencosud.sim.service.impl;

import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.cencosud.sim.mapper.OperacionMapper;
import com.cencosud.sim.model.request.CargaPcbsRequest;
import com.cencosud.sim.service.OperacionService;

@Service("OperacionService")
public class OperacionServiceImpl implements OperacionService{
	static final Logger log = Logger.getLogger(OperacionServiceImpl.class);
	
		@Autowired
		private OperacionMapper operacionMapper;

		@Override
		public int registrarErrores(Map<String, Integer> error) {
			try {
				log.info("registrarErrores("+error+")");
				return operacionMapper.registrarErrores(error);
			} catch (Exception e) {
				log.error(e);
			}
			return 0;
		}

		@Override
		public void quitarErrores(int txId) {
			try {
				operacionMapper.quitarErrores(txId);
			} catch (Exception e) {
				log.error(e);
			}
			
		}

		@Override
		public int validarComuna(CargaPcbsRequest cargaPcbsRequest) {
			return operacionMapper.validarComuna(cargaPcbsRequest);
		}

		@Override
		public Long obtenerErrores(int txId) {
			return operacionMapper.obtenerErrores(txId);
		}

		
		
}
