package com.cencosud.sim.service;

import java.util.List;

import com.cencosud.sim.model.RecepcionModel;
import com.cencosud.sim.model.request.HistorialRequest;

public interface RecepcionService {

	int registrarArchivoRespuesta(RecepcionModel recepcionModel);

	List<RecepcionModel> obtenerHistorial(HistorialRequest historialRequest);

	RecepcionModel obtenerRecepcion(int recId);

}
