package com.cencosud.sim.service;

import java.util.List;

import com.cencosud.sim.model.ItemPlantillaModel;

public interface ItemPlantillaService {

	List<ItemPlantillaModel> obtenerItemPlantillaFiltrado(List<ItemPlantillaModel> plantillasAprocesar);

	void guardarDatosPlantilla(List<ItemPlantillaModel> plantillasAGuardar);

}
