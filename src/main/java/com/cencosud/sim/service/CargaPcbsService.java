package com.cencosud.sim.service;

import java.util.List;

import com.cencosud.sim.model.CargaPcbsModel;
import com.cencosud.sim.model.RespuestaModel;
import com.cencosud.sim.model.request.CargaPcbsRequest;
import com.cencosud.sim.model.request.ExcelRequest;
import com.cencosud.sim.model.response.CargasPcbsResponse;

public interface CargaPcbsService {

	CargasPcbsResponse procesarCargaPcbs(CargaPcbsRequest cargaPcbsRequest);

	List<CargaPcbsModel> obtenerCargaPcbs(ExcelRequest excelRequest);

	void registrarIdEmail(List<RespuestaModel> respuestas, int nuevoId);
	
	CargaPcbsModel obtenerPrimeraCargaPcbsXProp(int txId, int idPropuesta, int idPlan);

}
