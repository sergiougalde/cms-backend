package com.cencosud.sim.model.request;

import java.io.Serializable;

public class CargaPcbsRequest implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int txId;
	private int tptxId;
	private String txFchInicio;
	private String txFchTermino;
	private int estado;
	private String usuario;
	public int getTxId() {
		return txId;
	}
	public void setTxId(int txId) {
		this.txId = txId;
	}
	public int getTptxId() {
		return tptxId;
	}
	public void setTptxId(int tptxId) {
		this.tptxId = tptxId;
	}
	public String getTxFchInicio() {
		return txFchInicio;
	}
	public void setTxFchInicio(String txFchInicio) {
		this.txFchInicio = txFchInicio;
	}
	public String getTxFchTermino() {
		return txFchTermino;
	}
	public void setTxFchTermino(String txFchTermino) {
		this.txFchTermino = txFchTermino;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CargaPcbsRequest [txId=");
		builder.append(txId);
		builder.append(", tptxId=");
		builder.append(tptxId);
		builder.append(", txFchInicio=");
		builder.append(txFchInicio);
		builder.append(", txFchTermino=");
		builder.append(txFchTermino);
		builder.append(", estado=");
		builder.append(estado);
		builder.append(", usuario=");
		builder.append(usuario);
		builder.append("]");
		return builder.toString();
	}
}
