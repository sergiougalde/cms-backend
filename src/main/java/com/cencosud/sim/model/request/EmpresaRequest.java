package com.cencosud.sim.model.request;

import java.io.Serializable;

public class EmpresaRequest implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String tipoCarga;

	public String getTipoCarga() {
		return tipoCarga;
	}

	public void setTipoCarga(String tipoCarga) {
		this.tipoCarga = tipoCarga;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmpresaRequest [tipoCarga=");
		builder.append(tipoCarga);
		builder.append("]");
		return builder.toString();
	}
	
}
