package com.cencosud.sim.model.response;

import java.io.Serializable;
import java.util.List;

import com.cencosud.sim.dto.Accion;

public class CargasPcbsResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int txId;
	private String tptxDescripcion;
	private String txFchInicio;
	private int cantidad;
	private String estCodigo;
	private String estDescripcion;
	private int estId;
	private List<Accion> accion;
	public int getTxId() {
		return txId;
	}
	public void setTxId(int txId) {
		this.txId = txId;
	}
	public String getTptxDescripcion() {
		return tptxDescripcion;
	}
	public void setTptxDescripcion(String tptxDescripcion) {
		this.tptxDescripcion = tptxDescripcion;
	}
	public String getTxFchInicio() {
		return txFchInicio;
	}
	public void setTxFchInicio(String txFchInicio) {
		this.txFchInicio = txFchInicio;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public String getEstCodigo() {
		return estCodigo;
	}
	public void setEstCodigo(String estCodigo) {
		this.estCodigo = estCodigo;
	}
	public String getEstDescripcion() {
		return estDescripcion;
	}
	public void setEstDescripcion(String estDescripcion) {
		this.estDescripcion = estDescripcion;
	}
	public int getEstId() {
		return estId;
	}
	public void setEstId(int estId) {
		this.estId = estId;
	}
	public List<Accion> getAccion() {
		return accion;
	}
	public void setAccion(List<Accion> accion) {
		this.accion = accion;
	}
	@Override
	public String toString() {
		return "CargasPcbsResponse [txId=" + txId + ", tptxDescripcion=" + tptxDescripcion + ", txFchInicio="
				+ txFchInicio + ", cantidad=" + cantidad + ", estCodigo=" + estCodigo + ", estDescripcion="
				+ estDescripcion + ", estId=" + estId + ", accion=" + accion + "]";
	}
	
}
