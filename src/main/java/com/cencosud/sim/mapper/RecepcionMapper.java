package com.cencosud.sim.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.cencosud.sim.model.RecepcionModel;
import com.cencosud.sim.model.request.HistorialRequest;

@Mapper
public interface RecepcionMapper {

	int registrarArchivoRespuesta(@Param("recepcionModel") RecepcionModel recepcionModel);

	List<RecepcionModel> obtenerHistorial(@Param("historialRequest") HistorialRequest historialRequest);

	RecepcionModel obtenerRecepcion(@Param("recId") int recId);

}
