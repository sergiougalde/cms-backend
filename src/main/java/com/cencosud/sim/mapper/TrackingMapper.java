package com.cencosud.sim.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import com.cencosud.sim.model.TrackingModel;
import com.cencosud.sim.model.request.TrackingRequest;

@Mapper
public interface TrackingMapper {

	List<TrackingModel> getTracking(@Param("trackingRequest") TrackingRequest trackingRequest);

	int obtenerIdOrigen(@Param("id") int id);

}
