package com.cencosud.sim.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.cencosud.sim.model.TransaccionModel;
import com.cencosud.sim.model.request.CargaPcbsRequest;
import com.cencosud.sim.model.response.CargasPcbsResponse;
import com.cencosud.sim.model.response.MessageResponse;

@Mapper
public interface TransaccionMapper {

	int registrarTransaccion(@Param("transaccionModel") TransaccionModel transaccionModel);

	int guardarTransaccion(@Param("transaccionModel") TransaccionModel transaccionModel);

	List<CargasPcbsResponse> obtenerTransacciones();

	CargasPcbsResponse obtenerTransaccion(@Param("txId") int txId);

	TransaccionModel obtenerTransaccionPadre(@Param("txId") int txId);

	TransaccionModel obtenerUltimaTransaccion(@Param("txId") int txId);

	MessageResponse obtenerMensaje(@Param("cargaPcbsRequest") CargaPcbsRequest cargaPcbsRequest);

	void guardarRutaArchivo(@Param("propuesta") int propuesta,@Param("nombre") String nombreArchivo,@Param("ruta") String ruta,@Param("txId") int txId);

}
