package com.cencosud.sim.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.cencosud.sim.model.CargaPcbsModel;
import com.cencosud.sim.model.RespuestaModel;
import com.cencosud.sim.model.request.CargaPcbsRequest;
import com.cencosud.sim.model.request.ExcelRequest;

@Mapper
public interface CargaPcbsMapper {

	Object realizarCargaPcbs(@Param("cargaPcbsRequest") CargaPcbsRequest cargaPcbsRequest);

	List<CargaPcbsModel> obtenerCargaPcbs(@Param("excelRequest") ExcelRequest excelRequest);

	void registrarIdEmail(@Param("list") List<RespuestaModel> list);
	
	List<CargaPcbsModel> obtenerCargaPcbsXProp(@Param("txId") int txId,
											   @Param("pptaId") int pptaId);
}
