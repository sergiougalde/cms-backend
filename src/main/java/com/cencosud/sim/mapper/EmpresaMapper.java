package com.cencosud.sim.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.cencosud.sim.model.EmpresaModel;
import com.cencosud.sim.model.request.EmpresaRequest;

@Mapper
public interface EmpresaMapper {

	List<EmpresaModel> obtenerEmpresaRecepcion(@Param("empresaRequest") EmpresaRequest empresaRequest);

	EmpresaModel obtenerEmpresaXID(@Param("empId") int empId);
	
	
	
}
